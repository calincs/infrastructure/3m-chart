# 3M-docker

This repo was forked from here: [https://gitlab.isl.ics.forth.gr/cci/3m-docker](https://gitlab.isl.ics.forth.gr/cci/3m-docker).

3M is a web tool that facilitates the schema mapping definition process by providing the appropriate environment that offers rich and complex functionality hidden under a simple and friendly user interface. 3M defines mapping definitions with respect to X3ML mapping definition language. In a nutshel, it 
(a) enables the definition of schema mappings in a collaborative manner,
(b) supports users with source and target schema analysis, 
(c) transforms source data to RDF with respect to the mapping definitions, and
(d) visualizes the RDF output.

The current repository provides the required docker resources for deploying 3M and its dependent applications (MongoDB and RdfVisualizer).

## Quick Setup

3M can be deployed easily in any machine with docker (20.10.1 and above) and docker-compose (version 1.25.0 and above) installed.
The quick setup can be completed in two steps

Clone the contents of the current gitlab repository

`git clone https://gitlab.isl.ics.forth.gr/cci/3m-docker.git`

Enter in the cloned folder and start 3M using docker-compose

`docker-compose up -d`

3M will be available under port 8082 (this can be altered through the docker-compose.yml, as it is shown below). 
Assuming that the IP address of the hosting machine running docker-compose is 0.0.0.0, 3M will be accessed through

`http://0.0.0.0:8082/3m`

## More Settings / Advanced Configuration

This section describes more settings as regards the configurability of 3M.

### Mailer settings

3M contains a mailer, which can be used is certain circumstances (e.g. if a user forgot his/her password). 
In order to use this feature, it is required to configure mailer with a valid e-mail account. 
To this end, the credentials and the details of the corresponding smtp should be filled in. 
This information is added in the `.env` file. 
An indicative example of the configuration for a gmail account can be found below.

```properties
spring_mail_host=smtp.gmail.com
spring_mail_port=587
spring_mail_username=my_username
spring_mail_password=my_super_secret_password
spring_mail_properties_mail_smtp_auth=true
spring_mail_properties_mail_smtp_ssl_trust=smtp.gmail.com
spring_mail_properties_mail_smtp_starttls_enable=true
mail_from=my_username@gmail.com
```

### RDFVisualizer settings

3M uses RDFVisualizer for depicting the transformed RDF resources in a user-friendly and interactive manner. 
In order to use RDFVisualizer, it is required to define the URL address were RDFVisualizer is deployed, in the 
`.env` file. 
The URL could be a combination of the host machine's IP address, 
the publicly available port (can be changed through the docker-compose.yml file), 
and the name of the web application (i.e. RDFVisualizer). 
Assuming that the IP of the hosting machine is 0.0.0.0 and the public port for RDFVisualizer is 8083 the 
corresponding configuration could be 

```properties
rdfvisualizer_url=0.0.0.0:8083/RDFVisualizer
```

### Change ports

By default 3m runs on port 8082, and RDFVisualizer on port 8083. 
This can be changed by updating the port mappings in the docker-compose.yml file.

### MongoDB public accessed

For security reasons, MongoDB connections are not publicly available. 
To enable public access to MongoDB, uncomment the port mappings from the docker-compose.yml file.
