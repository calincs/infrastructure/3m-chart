#!/bin/bash

# Copy policies and schemas if not present on the mounted volume
cp -nr /preloadedFiles/* /uploads

# Run 3M
java -Djava.security.egd=file:/dev/./urandom -Dfile.encoding=UTF-8 -jar 3mspring.jar \
	--server.port=8082 \
	--spring.mail.host=$spring_mail_host \
	--spring.mail.port=$spring_mail_port \
	--spring.mail.username=$spring_mail_username \
	--spring.mail.password=$spring_mail_password \
	--spring.mail.properties.mail.smtp.auth=$spring_mail_properties_mail_smtp_auth \
	--spring.mail.properties.mail.smtp.ssl.trust=$spring_mail_properties_mail_smtp_ssl_trust \
	--spring.mail.properties.mail.smtp.starttls.enable=$spring_mail_properties_mail_smtp_starttls_enable \
	--mail.from=$mail_from \
	--spring.data.mongodb.host=$spring_data_mongodb_host
	--rdfvisualizer.url=$rdfvisualizer_url
