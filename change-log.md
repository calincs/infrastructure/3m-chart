# Change Log

## 09 Septemper 2024: V 0.3.99

### Bug Fixes:
* Resolving issue with the XPaths, namespaces and generators;
* Resolving issues with engine execution and projects been opened from a link;

## 29 August 2024: V 0.3.98

### Bug Fixes:
* Resolving issue with the wizard of initiating new projects, not allowing getting to the second step due to authorization restrictions;
* Disabling the link of any project that currently is in draft state;

## 17 July 2024: V 0.3.95

### New or Improved Functionality:
* Resolving issue with project Description not been editable when too short;
* Validating Generator Definition's URI (well formed XML files), to avoid RDF issues;
* Guests can now remove themselves from the list of participants;
* The service/link to produce and get the contents of the X3ML of a mapping-project has become unsecured. 
i.e. http://<DOMAIN>/3m/x3ml/convertAndsaveX3mlByMappingProjectHrid/<HRID>
* A list of the XPaths is provided at the generator declaration, when XPath type is used for an argument;
* Minor UI improvements;
* Resolved generator declaration language related issues;
* A link for the mapping project is provided at the project list to be used with right click "open in new tab";

## 17 May 2024: V 0.3.9

### New or Improved Functionality:
* Introducing the copy / paste of a mapping into a mapping project, the same way as it's done for the link;
* Introducing the basic Explorer side left panel at the mapping project's workplace;
* Ignoring any validation of unchecked mappings or links while generating the X3ML or transforming the RDF;
* Possibe fix for action authorization issues;


## 17 April 2024: V 0.3.8

### New or Improved Functionality:
* D4Science authentication support;

## 01 April 2024: V 0.3.7

### New or Improved Functionality: 
* UI Fixes and minor issues;
* Supporting mapping project navigation from a URL by using the HRID (mapping project, mapping and link);
* Selecting which input source files to be used during the engine transformation;
* JWT support;
* New services provided, to get the X3ML and Generator Policy, given the HRID;

## 03 March 2023: V 0.3.6

### Library Update: 
* X3ML engine version has been updated;

## 26 October 2022: V 0.3.5

### New or Improved Functionality: 
* Errors on invalid links or domains are now spotted by auto-scrolling to them, when found;
* Minor visual improvements;

### Bug Fixes:
* Resolving issue with UUID not been saved some times;
* Prevents user from saving changes applied directly on X3ML code when links or mappings have been left unchecked such that they should be ignored when producing the X3ML;

## 30 September 2022: V 0.3.4

### New or Improved Functionality: 
* Displaying the first comment of mapping or link at the header. This feature is enabled by defaiult, however users can disable it through "User's Configuration Options";
* Allowing to change the order of the comments used in any mapping or link;
* Choosing which mappings or links will be included when producing the X3ML or the final RDF output (meaning that the unchecked ones will be ignored);
* Supporting the "enter" key press in dialog displayed content;
* Searching capabilities functionality has been improved;
* Adding Generator declarations functionality has been improved;

## 08 September 2022: V 0.3.3

### Bug Fixes:
* Resolving issue related with the produced output when named-graphs have been used in the mapping-project, causing the engine to require a restart;

## 06 September 2022: V 0.3.2

### Bug Fixes:
* Resolving issue with missing root XPath from the list of recomendations;

## 05 September 2022: V 0.3.1

### New or Improved Functionality: 
* Inactive preloaded file records can be hidden/shown by using a switch in the "Predefined File Management" view;
* Supporting the extra "language" argument when declaring any label generator declaration;
* Extra information is displayed on mapping and link's header when in the workspace view;
* Expanding many mappings simultaneously is allowed in workspace view;
* New checkbox for expanding/collapsing all mappings at once has been introduced in the workspace view;
* Improved the UI such as long XPaths are better displayed;

### Bug Fixes:
* Resolving issue related to the generator declarations and the X3ML file;
* Resolving issue with not displaying errors occurring when gathering the namespaces of RDFS files;
* Resolving issue with not properly importing form ZIP file when the contained files are placed in sub-folders;
* Resolving issue with no properly supporing namespaces used within elements in the XML source files, when listing the XPaths;
* Resolving issue with new preloaded files in the management view, been persisted twice in the database in some cases;

## 29 July 2022: V 0.3

### New or Improved Functionality: 
* Searching functionality on user management view has been improved;

### Bug Fixes:
* Resolving issue where the system failing into adding instance or label generators within native additional elements in certain cases;
* Improving the UI of the Workspace view such that the "participants" icon button is easier found; 
* Resolving issue with missing XPaths at the link's list of available XPaths for the source relation and node;

## 27 July 2022: V 0.2.9

### Bug Fixes:
* Resolving issue related to conflicts between intermediate and additional elements while importing any mapping from X3ML code (fragment code import, whole X3ML import, Zip file import);
* Resolving issue related to adding native additional in certain cases;

## 25 July 2022: V 0.2.8

### New or Improved Functionality: 
* Adding support for producing turtle, N-Triples & TriG output files as well;

### Bug Fixes:
* Resolving issue with not been able to produce the RDF output in certain cases;

## 30 June 2022: V 0.2.7

### New or Improved Functionality: 
* Importing and deleting generator definitions has been improved;
* Minor updates in the content of some emails sent by the system to users;
* Resetting the contents of the RDF Visualizer's dialog every time it is opened;
* Several minor UI improvements;

### Bug Fixes:
* Resolving issue with not sending (through email) user's credential when requested;
* Resolving all issues with deleting groups and group-sets, affecting the contained mappings;

## 16 June 2022: V 0.2.6

### Bug Fixes:
* Resolving issues with long text values not been broken into lines;
* Resolving issue with not properly importing from ZIP in some cases;
* Huge performance improvement on rendering;

## 14 June 2022: V 0.2.5

### Bug Fixes:
* Resolving issue with confirmation snackbar and some deprecated property;
* Resolving issue with deprecated property on Grid element;

## 08 June 2022: V 0.2.4

### Bug Fixes:
* Resolving issue with instance info & instance generator elements been added when they shouldn't, while importing an X3ML file in some cases;
* Resolving issue with not loading changes applied on class instances, to be used in the RDF Visualiser;

## 06 June 2022: V 0.2.3

### Bug Fixes:
* Resolving issue with new registered users not been able to login after been activated;
* Minor improvements;

## 16 May 2022: V 0.2.2

### Bug Fixes:
* Resolving issues related with the management of source or target schema namespaces and importing from ZIP file;

## 14 April 2022: V 0.2.1

### New or Improved Functionality: 
 * Certain users are informed through email about any “User Registration” submission;  
   For that purpose a new user role has been introduced;  
   Feedback message is now provided by the system to the new applicant;
 * The user administration section has been enriched with additional functionality;  
   The option of sending email to users has been added;  
   A pre-composed email content that takes into account recipient and sender’s information is provided when a user has just been enabled;
 * A new “About” section has been introduced, providing information about the system;
 * Improving the “Predefined File Management” section by listing the namespaces of the target schemata;  
   A reset functionality is also provided;  
   The list of namespaces is automatically provided after uploading a new target schema;  
   Administrators can fully manage the list of namespaces of any target schema (edit, add and delete);
 * Improving visual aspects in several sections of the system;
 * Several dependency libraries of the system have been updated;

### Bug Fixes:
* Resolving issue with users still been able to delete mapping projects of others;
* Resolving issue with some of the current predefined target schemata not listing their namespaces in some cases;

## 28 March 2022: V 0.2

### New or Improved Functionality: 

 * A copy paste capability for links between different mapping projects has been introduced.  
   Validation of the pasted links is automatically applied, preventing possible mistakes.  
   The only restriction is that the same browser has to be used and that it doesn't work between regular and incognito tabs;
 * Several libraries used by the system have been updated and several parts of the system have been refactored;
 * Improved the way pagination and search filtering work together when at the mapping project list view;
 * Filtering at the mapping project list view, has been improved to allow searching in all fields simultaneously;
 * The functionality of the “Files’ Metadata” manager dialog has significantly been improved.  
   Among several of these improvements, stands out the   capability of adding or removing namespaces from any source input or target schema file;
 * Fully supporting native “Additionals” has been introduced (native are the “Additionals” that appear within other “Additionals”). This functionality is offered through a new designed dialog that allows navigation among the children of any parent “Additional” at any level of depth. All the known functionality of the non-native “Additionals” is also available for the native ones (that includes the respective instance and label generators, variable and instance info, along with any validation that can be applied on relation and entity fields);
 * Introducing new component for managing the predefined “Target Schema” and “Generator Policy” files (concerns administrators);
   That new component allows:
   * Enabling or disabling any predefined file from been used by users;
   * Editing the metadata information of any predefined file and any instance file based on that;
   * Uploading new files to be used as predefined by any user;

### Bug Fixes:

 * Resolving issue with properly displaying the participation and authorship of any user through the user management view (concerns administrators);
 * Resolving issue with not automatically using file’s name as title for new uploaded source input and target schema files when using the “Files’ Metadata” manager dialog;
 * Resolving issue with changes applied within the X3ML code of the:  
&nbsp;&nbsp; info --> source --> source-info --> namespaces, and  
&nbsp;&nbsp; info --> target --> target-info --> namespaces elements not been loaded through the UI;
* Resolving issue with not been able to construct the generator policy file out of the mapping project’s list of generator definitions, after having used the "Use UUID" optional filed on any of them;
* Resolving issue with the information displayed at the final step of the wizard for the generator policy file, while initiating a new project mapping;
* Resolving issue with error message not been properly shown when importing X3ML fragment code to Domain or link;
* Resolving issue with list of objects not been available through domain’s target entity field, after updating that domain through X3ML fragment code;
* Resolving issue with not properly rendering the errors after validating the selected values of some fields;
* Resolving several user syncing related errors;

## 21 December 2021: V 0.1

### New or Improved Functionality: 

 * Providing "Source Input Coverage" through a Doughnut Chart and managing the XPaths to be taken into account;
 * validating selected values for the "Source Node" and "Source Relation" of the domain, the links or the intermediates;
 * The way conditions are displayed under "Source Relation" in either observe or edit view, has been improved;
 * Disallow replacing files from the fileMetadata manager or the wizard;
 * Allowing canceling while validating the selected values;
 * Shorten vertical space between components, such that more items are viewable in the same screen;
 * Adding button that marks any erroneous value (value that was not found in the available options) in the mapping, link, intermediate or addition as custom;
 * Supporting no namespace schema location validation of the engine;
 * Reordering links by dragging them up or down, within the same mapping;
 * Providing new "Clone" functionality for the links within the sae mapping;

### Bug Fixes:

 * Resolving issue with fileMetadata dialog been saved when an input source fileMetadata item is empty;
 * Resolving issue with cases where formMetadata is null;
 * Providing feedback to the user in case of missing or invalid generator policy file, while importing from a ZIP file;
 * Resolving issue where no view is displayed for the remote server when the URL Address is set to ".../3m", while the user is logged in;
 * Resolving issue with opening Mapping Project that has no input sources nor target Schemata;
 * Resolving issue with not properly updating the namespaces of the fileMetadata that have been synced from other users;
 * Resolving issue with false tracking changes applied on fileMetadata after they have been synced by other users;
 * Resolving issue with Firefox and UI been changed when hovering over fileMetadata to be deleted;
 * Resolving issue with namespaces not been listed when setting predefined target schema at the fileMetadata dialog;
 * Preventing system from deselecting all the XPaths from the list of XPaths to be taken into account when calculating the "Source Coverage" when there is not any XPath used yet and the user clicks on the "deselect all" button;
 * Making the behavior of the system more stable when deciding whether to delete or not a physical file from the server;
 * Resolving issue while managing the XPaths to be covered in a new (empty) mapping project;
 * Initializing models for others that are synced, when a preloaded target schema is selected for an empty fileMetadata at the fileMetadata manager;
 * Preventing the user from loading the main workspace page without having selected some MappingProject first (within that same browser's tab), by redirecting him to the project list page;
